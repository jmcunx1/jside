# for jside
#
# Copyright (c) 2022 ... 2024 2025
#     John McCue
#
# Permission to use, copy, modify, and distribute this software
# for any purpose with or without fee is hereby granted, provided
# that the above copyright notice and this permission notice
# appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
# NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# This file should *not* need to be changed unless you need to
# customize some settings for another OS.
# build.sh will take care of the # settings for you.  Execute:
#    ./build.sh --help
# to see compile options
#

SHELL=/bin/sh

#--- where to install
DESTDIR=LOCATION
PRODUCTION=$(DESTDIR)/bin
PRODPMAN=$(DESTDIR)/man
PRODMAN=$(PRODPMAN)/man1
PRODREADH=$(DESTDIR)/share
PRODREADME=$(PRODREADH)/jside

#>>>>>>> select desired OS
#--- Linux 64 bit
#LINUX64#WALL=-Wall -Wextra -Wuninitialized -m64
#LINUX64#ETAGS=etags
#LINUX64#GZ=.gz
#LINUX64#NROFF=cat
#LINUX64#OSDESC != uname -smr

#--- Linux 32 bit
#LINUX32#WALL=-Wall
#LINUX32#ETAGS=etags
#LINUX32#GZ=.gz
#LINUX32#NROFF=cat
#LINUX32#OSDESC != uname -smr

#--- BSD 64 bit
#BSD64#WALL=-Wall -Wextra -Wuninitialized -m64
#BSD64#ETAGS=etags
#BSD64#GZ=.gz
#BSD64#NROFF=cat
#BSD64#OSDESC != uname -smr

#--- BSD 32 bit
#BSD32#WALL=-Wall -Wextra -Wuninitialized
#BSD32#ETAGS=etags
#BSD32#GZ=.gz
#BSD32#NROFF=cat
#BSD32#OSDESC != uname -smr

#--- AIX specific
#AIX#WALL=-bnoquiet
#AIX#ETAGS=true
#AIX#GZ=
#AIX#NROFF=nroff -man
#AIX#OSDESC = `uname -a`

#--- should not have to change these
CC=cc
CHMOD=chmod
CP=cp
CTAGS=ctags
ECHO=echo
GZIP=gzip
LINK=cc
#FOUND_JLIB#LIBS=-lj_lib2
#NO_JLIB#LIBS=
#FOUND_JLIB#HJLIB=-DHAVE_JLIB
MV=mv
RM=rm -f
RMDIR=rmdir
STRIP=strip
MKDIR=mkdir

EXE=
OBJ=.o

#--- for prod
#FOUND_JLIB#CFLAGS=-O1 -c $(WALL) $(HJLIB) -IINCJLIB -DOSTYPE="\"$(OSDESC)\""
#FOUND_JLIB#LFLAGS=-O1 $(WALL) -LJLIBLOC -o jside $(LIBS)
#NO_JLIB#CFLAGS=-O1 -c $(WALL) -DOSTYPE="\"$(OSDESC)\""
#NO_JLIB#LFLAGS=-O1 $(WALL) -o jside $(LIBS)

#--- for valgrind
#    OR for OpenBSD ktrace/kdump
#VALGRIND##FOUND_JLIB#CFLAGS=-O0 -g -c $(WALL) $(HJLIB) -IINCJLIB -DOSTYPE="\"$(OSDESC)\""
#VALGRIND##FOUND_JLIB#LFLAGS=-O0 -g $(WALL) -LJLIBLOC -o jside $(LIBS)
#VALGRIND##NO_JLIB#CFLAGS=-O0 -g -c $(WALL) -DOSTYPE="\"$(OSDESC)\""
#VALGRIND##NO_JLIB#LFLAGS=-O0 -g $(WALL) -o jside $(LIBS)

#--- continue on
#NO_JLIB#JCC=jside_j.c
#NO_JLIB#JCOB=jside_j$(OBJ)

ALL_OBJ=jside$(OBJ) jside_h$(OBJ) jside_i$(OBJ) $(JCOB) jside_m$(OBJ) jside_u$(OBJ)
ALL_C=jside.c jside_h.c jside_i.c $(JCC) jside_m.c jside_u.c
ALL_H=jside.h

#
# do the work
#
all:	ckenv tags $(ALL_OBJ) jside.1$(GZ)
	$(LINK) $(LFLAGS) $(ALL_OBJ) $(LIBS)

ckenv:
	echo checking Variable DESTDIR
	test $(DESTDIR)

tags:	$(ALL_H) $(ALL_C)
	-$(CTAGS) $(ALL_H) $(ALL_C)
	-$(ETAGS) $(ALL_H) $(ALL_C)

jside.1.gz:	jside.1
	-$(RM) jside.1.gz
	$(GZIP) jside.1

jside.1:	jside.man
	$(NROFF) jside.man > jside.1

#--- AIX install(1) is odd compared to BSDs and Linux
install:	all
	-$(MKDIR) $(PRODUCTION)
	-$(MKDIR) $(PRODPMAN)
	-$(MKDIR) $(PRODMAN)
	-$(CHMOD) 755 $(PRODUCTION)
	-$(CHMOD) 755 $(PRODPMAN)
	-$(CHMOD) 755 $(PRODMAN)
	-$(STRIP) jside
	$(CP) jside $(PRODUCTION)/jside
	$(CHMOD) 755 $(PRODUCTION)/jside
	$(CP) jside.1$(GZ) $(PRODMAN)/jside.1$(GZ)
	$(CHMOD) 644 $(PRODMAN)/jside.1$(GZ)
	-$(MKDIR) $(PRODREADH)
	-$(MKDIR) $(PRODREADME)
	-$(CHMOD) 755 $(PRODREADH)
	-$(CHMOD) 755 $(PRODREADME)
	$(CP) README.md $(PRODREADME)/README.md
	$(CHMOD) 644 $(PRODREADME)/README.md
	$(CP) LICENSE.md $(PRODREADME)/LICENSE.md
	$(CHMOD) 644 $(PRODREADME)/LICENSE.md

uninstall:	all
	-$(RM) $(PRODUCTION)/jside
	-$(RM) $(PRODMAN)/jside.1.gz
	-$(RM) $(PRODMAN)/jside.1
	-$(RM) $(PRODREADME)/README.md
	-$(RM) $(PRODREADME)/LICENSE.md
	-$(RMDIR) $(PRODREADME)

clean:
	-$(RM) *$(OBJ)
	-$(RM) jside$(EXE)
	-$(RM) jside.1
	-$(RM) jside.1.gz
	-$(RM) TAGS
	-$(RM) tags
	-$(RM) core
	-$(RM) *.core
	-$(RM) a.out
	-$(RM) *.pdb
	-$(RM) *.ilk
	-$(RM) *.bak
	-$(RM) Makefile
	-$(RM) ktrace.out

### END
