## jside - Print Delimited File Sideways

jside(1) is a simple utility that converts a Delimited
Text File from Horizontal Format to Vertical Format.
Headings will end up in Column 1.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jside) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jside.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jside.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
